import Vue from 'vue'
import App from './SenseSelectionBarVue.vue'
import store from './store'

Vue.config.productionTip = false

import QlikFieldStyles from "sense-field-vue/dist/sense-field-vue.css";

new Vue({
  store,
  render: h => h(App)
}).$mount('#senseselectionbarvue')
